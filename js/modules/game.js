
export default class Game extends Phaser.Scene
{
	constructor(scene)
	{
		super('GameScene');

		this.player;
		this.walkSpeed = 160;
		this.direction = 0;
		this.platforms;
		this.stars;
		this.bombs;

		this.cursors;
		this.leftButton;
		this.rightButton;
		this.jumpButton;

		this.score = 0;
		this.scoreText = `Score: 0`;

		this.gameOver = false;
	}

	preload()
	{
		this.load.image('sky', 'assets/sky.png');
		this.load.image('ground', 'assets/platform.png');
		this.load.image('star', 'assets/star.png');
		this.load.image('bomb', 'assets/bomb.png');
		this.load.spritesheet('dude', 'assets/dude.png',
			{ frameWidth: 32, frameHeight: 48 });
	}

	create()
	{
		this._platforms();
		this._player();
		this._cursors();
		this._checkDevice();
		this._buttons();
		this._stars();
		this._score();
		this._bombs();

		this.launchBomb();
	}

	update()
	{
		if (this.gameOver)
			this.restart();
		this._goUpdate();
	}

	collectStar(player, star)
	{
		star.disableBody(true, true);

		this.score += 10;
		this.scoreText.setText(`Score: ${this.score}`);

		if (this.stars.countActive(true) == 0)
		{
			this.stars.children.iterate(child => {
				child.enableBody(true, child.x, 0, true, true);
			});
			this.launchBomb();
		}
	}

	jump()
	{
		this.player.setVelocityY(-330);
	}

	tryJump()
	{
		if (this.player.body.touching.down)
			this.jump();
	}

	/* Если число положительное - игрок идет вправо,
		* если отрицательное - игрок идет влево.
		* Если direction == 0 - игрок останавливается. */
	go(direction)
	{
		this.direction = Math.sign(direction);
	}

	_goUpdate()
	{
		switch (this.direction)
		{
			case 1:
				this.player.anims.play('right', true);
				this.player.setVelocityX(this.walkSpeed);
				break;

			case -1:
				this.player.anims.play('left', true);
				this.player.setVelocityX(-this.walkSpeed);
				break;

			case 0:
				this.player.anims.play('turn', true);
				this.player.setVelocityX(0);
				break;

		}
	}

	restart()
	{
		this.gameOver = false;
		this.time.addEvent(
		{
				delay: 500,
				callback: () =>
			{
				alert('Упс... Вы словили маслину! :)');
				this.scene.restart();
			},
				callbackScope: this
		});
	}

	stop()
	{
		this.direction = 0;
	}

	hitBomb(player, bomb)
	{
		player.setTint(0xff0000);
		player.anims.play('turn');
		this.physics.pause();
		this.gameOver = true;
	}

	launchBomb()
	{
		let x = (this.player.x < 400) ? Phaser.Math.Between(400, 800)
		    : Phaser.Math.Between(0, 400),
				bomb = this.bombs.create(x, 16, 'bomb');

		bomb.setBounce(1);
		bomb.setCollideWorldBounds(true);
		bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
	}


	/* Методы обработки сущностей */
	_platforms()
	{
		this.add.image(400, 300, 'sky');

		this.platforms = this.physics.add.staticGroup();

		this.platforms.create(400, 568, 'ground')
			.setScale(2)
			.refreshBody();
		this.platforms.create(600, 400, 'ground');
		this.platforms.create(50, 250, 'bomb')
			.setScale(3)
			.refreshBody();
		this.platforms.create(750, 220, 'ground');
		this.platforms.create(400, 10, 'ground');
	}

	_player()
	{
		this.player = this.physics.add.sprite(
			100, 40, 'dude');
		//this.player.setBounce(0.1);
		this.player.setCollideWorldBounds(true);

		this.anims.create
		({
			key: 'left',
			frames: this.anims.generateFrameNumbers(
				'dude', { start: 0, end: 3 }),
			frameRate: 10,
				repeat: -1
		});
		this.anims.create
		({
			key: 'turn',
			frames: [ { key: 'dude', frame: 4 } ],
			frameRate: 20
		});
		this.anims.create
		({
			key: 'right',
			frames: this.anims.generateFrameNumbers(
			'dude', { start: 5, end: 8 }),
		});

		this.physics.add.collider(this.player, this.platforms);
	}

	_cursors()
	{
		this.cursors = this.input.keyboard.createCursorKeys();

		this.cursors.left.on('down', () => this.go(-1));
		this.cursors.right.on('down', () => this.go(1));
		this.cursors.up.on('down', () => this.tryJump());

		this.cursors.left.on('up', () =>
		{
			if (this.direction == -1) this.stop();
		});
		this.cursors.right.on('up', () =>
		{
			if (this.direction == 1) this.stop();
		});
	}

	_checkDevice()
	{
		if (this.game.device.os.desktop)
			document.querySelectorAll('.movement-button').forEach(button =>
			{
				button.style.display = 'none';
			});
	}

	_buttons()
	{
		this.leftButton = document.getElementById('movement-button-left');
		this.rightButton = document.getElementById('movement-button-right');
		this.jumpButton = document.getElementById('movement-button-jump');

		this.leftButton.onmousedown =
		this.leftButton.ontouchstart = () => this.go(-1);
		this.rightButton.onmousedown =
		this.rightButton.ontouchstart = () => this.go(1);
		this.jumpButton.onmousedown =
		this.jumpButton.ontouchstart = () => this.tryJump();


		this.leftButton.onmouseup =
		this.leftButton.ontouchend = () => this.stop();
		this.rightButton.onmouseup =
		this.rightButton.ontouchend = () => this.stop();
	}

	_stars()
	{
		this.stars = this.physics.add.group
		({
			key: 'star',
			repeat: 11,
			setXY: { x: 12, y: 0, stepX: 70 }
		});
		this.stars.children.iterate(child => {
			child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
		});

		this.physics.add.collider(this.platforms, this.stars);
		this.physics.add.overlap(this.player, this.stars,
		    this.collectStar, null, this);
	}

	_score()
	{
		this.scoreText = this.add.text(
			16, 16, this.scoreText,
			{ fontSize: '32px', fill: '#000' }
		);
	}

	_bombs()
	{
		this.bombs = this.physics.add.group();

		this.physics.add.collider(this.bombs, this.platforms);
		this.physics.add.collider(
			this.player, this.bombs, this.hitBomb, null, this
		);
	}
/* Конец | Методы обработки сущностей */

}

