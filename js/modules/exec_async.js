
export default async function exec_async(func)
{
	return new Promise((resolve, reject) => resolve(func()));
}

