import Game from './modules/game.js'


export default class MW_Map extends Phaser.Game
{
	constructor(config)
	{
		super(config);
	}
}

window.onload = () =>
{
	let config =
	{
		type: Phaser.AUTO,

		scale:
		{
			mode: Phaser.Scale.FIT,
			parent: 'MW-MAP',
			autoCenter: Phaser.Scale.CENTER_BOTH,
			width: 800,
			height: 600
		},

		physics:
		{
			default: 'arcade',
			arcade: {
				gravity: { y: 300 },
				debug: false
			}
		},

		scene: [Game]
	};

	new MW_Map(config);
}
